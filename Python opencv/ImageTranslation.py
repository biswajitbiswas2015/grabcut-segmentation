# coding=utf-8
from __future__ import print_function
import cv2
import numpy as np
import  matplotlib as plt


def main():
    import cv2
    import numpy as np
    img = cv2.imread('pic11.jpg', cv2.IMREAD_COLOR)
    num_rows, num_cols = img.shape[:2]
    print(num_rows,num_cols,end='\n')
    translation_matrix = np.float32([[1, 0, 70], [1, 1, 110]])
    print(translation_matrix)
    img_translation = cv2.warpAffine(img,translation_matrix,
                                     (num_cols,num_rows), cv2.INTER_LINEAR)

    img_output = np.zeros(img.shape, dtype=img.dtype)
    for i in range(num_rows):
        for j in range(num_cols):
            offset_x = int(25.0 * np.math.sin(2 * 3.14 * i / 180))
    offset_y = 0
    if j + offset_x < num_cols:
        img_output[i, j] = img[i, (j + offset_x) % num_cols]
    else:
        img_output[i, j] = 0

    cv2.namedWindow('Translation', cv2.WINDOW_FREERATIO)
    cv2.moveWindow('Translation', 450, 0)
    cv2.namedWindow('Source', cv2.WINDOW_FREERATIO)
    cv2.moveWindow('Source', 10, 0)
    cv2.imshow('Source', img)
    cv2.imshow('Translation', np.uint8(img_output))
    cv2.waitKey()
    cv2.destroyAllWindows()


if __name__ == "__main__":
    main()






# grabcut segmentation

GrabCut is an image segmentation method based on graph cuts. Starting with a user-specified bounding box around the object to be segmented, the algorithm estimates the colour distribution of the target object and that of the background using a Gaussian